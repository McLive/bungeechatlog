package eu.mclive.BungeeChatLog;

import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.config.Configuration;
import eu.mclive.BungeeChatLog.MySQL.*;

public class BungeeChatLog extends Plugin {
	
	private static final Level SEVERE = null;
	public static BungeeChatLog INSTANCE;
	public final Logger logger = getLogger();
	public static MySQL sql;
	public MySQLHandler sqlHandler;
	public ChatLogConfiguration config;
	Long pluginstart = null;
	public static boolean debug;
	
	public void onEnable() {
		BungeeChatLog.INSTANCE = this;

		//Needed to create the config. Not the whole debug part but yea.
		//Just the getConfiguration() call.
		debug = getConfiguration().getConfig().getBoolean("debug");
		
		try {
			System.out.println("Loading MySQL ...");
			sql = new MySQL(this);
			System.out.println("Loading MySQLHandler ...");
			sqlHandler = new MySQLHandler(sql);
			//startRefresh();
			System.out.println("MySQL successfully loaded.");
		} catch (Exception e1) {
			System.out.println("Failled to load MySQL: " + e1.toString());
		}
		
		ProxyServer.getInstance().getPluginManager().registerCommand(this, new ChatReport(this));
		this.getProxy().getPluginManager().registerListener(this, new Events(this));
		
		cleanup();
		
        Date now = new Date();
        pluginstart = new Long(now.getTime()/1000);
		
	}

	public ChatLogConfiguration getConfiguration(){
		if(this.config == null) config = new ChatLogConfiguration(this);
		return this.config;
	}
	
	public Configuration getConfig(){ return getConfiguration().getConfig(); }
	
	public void saveConfig(){ getConfiguration().saveConfig(); }
	
	public void cleanup() {
		boolean doCleanup = getConfig().getBoolean("Cleanup.enabled");
		int since = getConfig().getInt("Cleanup.since");
		
		if(doCleanup) {
			System.out.println("Doing Cleanup...");
			Calendar cal = Calendar.getInstance();
			Date now = new Date();
			cal.setTime(now);
			cal.add(Calendar.DATE, -since); // subtract days from config
		
			final Long timestamp = cal.getTimeInMillis() / 1000L;

			ProxyServer.getInstance().getScheduler().runAsync(this, new Runnable() {
		    	public void run() {
		    		sqlHandler.delete(timestamp);
		    	}
		    });
		    
		} else {
			System.out.println("Skipping Cleanup because it is disabled.");
		}
	}
	
	/*
	public void startRefresh() {
		Bukkit.getScheduler().runTaskTimerAsynchronously(this, new Runnable() {
			public void run() {
				try {
					sql.refreshConnect();
				} catch (Exception e) {
					logger.warning("Failled to reload MySQL: " + e.toString());
				}
			}
		}, 20L*10, 20L * 1800);
	}
	*/
}
