package eu.mclive.BungeeChatLog;

import java.util.Date;

import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.ChatEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

public class Events implements Listener {
	private BungeeChatLog plugin;
	
	public Events(BungeeChatLog plugin) {
		this.plugin = plugin;
	}
	
	@EventHandler
	public void onChat(ChatEvent e) {
		final ProxiedPlayer p = (ProxiedPlayer) e.getSender();
		final String msg = e.getMessage();
		final String server = p.getServer().getInfo().getName();
		System.out.println(p + " " + msg + " " + server);
		
		ProxyServer.getInstance().getScheduler().runAsync(this.plugin, new Runnable() {
			@Override
	        public void run() {
	            Date now = new Date();
	            Long timestamp = new Long(now.getTime()/1000);
	            if(!msg.startsWith("/")) {
	            	try {
	            		plugin.sqlHandler.addMessage(server, p, msg, timestamp);
	            	} catch (NullPointerException e) {
	            		System.out.println("[BungeeChatLog] Lost MySQL connection! Message wasn't logged. Please check bungee_config.yml");
	            	}
	            }
	        }
		});
		
	}
	
}
